package pl.klkl.maps;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

import static org.fest.assertions.api.Assertions.assertThat;

public class DCompute {
    final String kawasakiKey = "Kawasaki";
    final String hondaKey = "Honda";
    final String suzukiKey = "Suzuki";

    Map<String, Integer> map;

    @BeforeMethod
    public void beforeMethod(){
        map = new HashMap<>();
        map.put(kawasakiKey, 3);
        map.put(hondaKey, 2);
    }

    @Test
    public void preJava8Implementation(){
        getValueFromMap(kawasakiKey);
        getValueFromMap(hondaKey);
        getValueFromMap(kawasakiKey);
        getValueFromMap(suzukiKey);

        assertThat(map).hasSize(3);
        assertThat(map.get(kawasakiKey)).isEqualTo(5);
        assertThat(map.get(hondaKey)).isEqualTo(3);
        assertThat(map.get(suzukiKey)).isEqualTo(1);
    }

    private Integer getValueFromMap(String key) {
        Integer oldValue = map.get(key);
        Integer newValue = oldValue == null ? 1 : 1 + oldValue;
        return map.put(key, newValue);
    }

    final BiFunction<String, Integer, Integer> computeFunction =
            (key,value) -> (value == null) ? 1 : value + 1;

    @Test
    public void usingCompute(){
        //when
        map.compute(kawasakiKey, computeFunction);
        map.compute(hondaKey, computeFunction);
        map.compute(kawasakiKey, computeFunction);
        map.compute(suzukiKey, computeFunction);

        //then
        assertThat(map.get(kawasakiKey)).isEqualTo(5);
        assertThat(map.get(hondaKey)).isEqualTo(3);
        assertThat(map.get(suzukiKey)).isEqualTo(1);
    }


    final BiFunction<String, Integer, Integer> forExistingElements =
            (key,value) -> value + 1;

    @Test
    public void usingComputeVariations(){
        //when
        map.computeIfPresent(kawasakiKey, forExistingElements);
        map.computeIfPresent(hondaKey, forExistingElements);
        map.computeIfPresent(kawasakiKey, forExistingElements);
        map.computeIfPresent(suzukiKey, forExistingElements);

        //then
        assertThat(map.get(kawasakiKey)).isEqualTo(5);
        assertThat(map.get(hondaKey)).isEqualTo(3);
        assertThat(map.get(suzukiKey)).isEqualTo(null);
    }

    final BiFunction<Integer, Integer, Integer> withAddedValue = null; //TODO Implement me !

    @Test
    public void usingMergeWithCustomValue(){
        //when
        map.merge(kawasakiKey, 2 ,withAddedValue);
        map.merge(hondaKey, 3, withAddedValue);
        map.merge(kawasakiKey, 1, withAddedValue);
        map.merge(suzukiKey, 1, withAddedValue);

        //then
        assertThat(map.get(kawasakiKey)).isEqualTo(6);
        assertThat(map.get(hondaKey)).isEqualTo(5);
        assertThat(map.get(suzukiKey)).isEqualTo(1);
    }
}
