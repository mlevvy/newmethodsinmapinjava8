package pl.klkl.maps;

import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static org.fest.assertions.api.Assertions.assertThat;

public class CRemove {
    final String key = "Kawasaki";
    final Integer value = 3;

    @Test
    public void preJava8Implementation(){
        //given
        Map<String, Integer> map = new HashMap<>();
        map.put(key, value);

        //when
        if (map.containsKey(key) && map.get(key).equals(value)) {
            map.remove(key);
        }

        //then
        assertThat(map).isEmpty();
    }

    @Test
    public void java8Implementation(){
        //given
        Map<String, Integer> map = new HashMap<>();
        map.put(key, value);

        //when
        map.remove(key, 7);

        //then
        assertThat(map.get(key)).isEqualTo(value);

        //when
        map.remove(key, value);

        //then
        assertThat(map).isEmpty();
    }

}
