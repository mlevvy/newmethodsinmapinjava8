package pl.klkl.maps;

import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static org.fest.assertions.api.Assertions.assertThat;

public class BPutIfAbsent {
    final String key = "Kawasaki";

    @Test
    public void preJava8Implementation(){
        //given
        Map<String, Integer> map = new HashMap<>();

        //when
        if (!map.containsKey(key))
            map.put(key, 1);

        if (!map.containsKey(key))
            map.put(key, 2);

        //then
        assertThat(map.get(key)).isEqualTo(1);
    }

    @Test
    public void java8Implementation(){
        //given
        Map<String, Integer> map = new HashMap<>();

        //when
        map.putIfAbsent(key, 1);
        map.putIfAbsent(key, 2);

        //then
        assertThat(map.get(key)).isEqualTo(1);
    }

}
