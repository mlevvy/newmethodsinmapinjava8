package pl.klkl.maps;

import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class ABasics {

    @Test
    public void iterate(){
        Map<String, Integer> map = new HashMap<>();
        map.put("Kawasaki", 3);
        map.put("Honda", 1);

        map.forEach(
            (key, value) ->
            System.out.println(String.format("For key '%s', value is '%s'", key, value))
        );
    }
}
